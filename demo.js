function resizeMap(){
    var s = document.getElementsByTagName("svg")[0];
    s.style.height = window.innerHeight;
    if (s.style.width > window.innerWidth){
        s.style.width = window.innerWidth;
    }
    // document.getElementsByTagName("svg")[0].style.height = window.innerHeight;    
}

function colorPlots(){
    resizeMap();
    var paths = document.getElementsByTagName("path");
    for(var i = 0; i < paths.length; i++){
        var path = paths.item(i);
        path.style.fill = getRandomColorHex();
    }
}

function highlightAugustaBitner(){
    resizeMap();
    var path = document.getElementById("P").style.fill = "#226";
}

function getRandomColorHex(){
    var color = "#" + getRandomColorComponent().toString(16) + getRandomColorComponent().toString(16) + getRandomColorComponent().toString(16);
    return color;
}

function getRandomColorComponent(){
    return Math.floor(Math.random() * 16);
}

function initializeMap(){
    console.log("initializeMap");
    d3.xml("map-web.svg")
        .then(data => {
            d3.select('div#map').node().append(data.documentElement);
            d3.select("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", "0 0 2450 1840")
                .classed("svg-content-responsive", true);
        });
}

initializeMap();
